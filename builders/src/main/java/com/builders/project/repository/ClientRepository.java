package com.builders.project.repository;

import com.builders.project.modal.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends JpaRepository<Client, Integer> {
    Page<Client> findByNameContainingOrCpfContaining(String name, String cpf, Pageable pageable);
}
