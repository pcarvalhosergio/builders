package com.builders.project.service;

import com.builders.project.modal.Client;
import com.builders.project.repository.ClientRepository;
import com.builders.project.util.Validation;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Service
public class ClientService {

    @Autowired
    private ClientRepository repository;

    private SimpleDateFormat format;

    public ClientService() {
        format = new SimpleDateFormat("MM/dd/yyyy");
    }

    public String find(String term, Integer size, Integer pageNumber) {
        String error = Validation.checkField("term", term);
        error += Validation.checkField("size", size);
        error += Validation.checkField("pageNumber", pageNumber);

        if(!error.isEmpty()) {
            return error;
        }

        ArrayList<Client> clients;
        int total;

        try {
            Page<Client> page = repository.findByNameContainingOrCpfContaining(term, term, PageRequest.of(pageNumber, size));
            clients = new ArrayList<>(page.getContent());
            total = page.getTotalPages();
        } catch (Exception e) {
            return "Error " + e.getMessage() + ". ";
        }

        return toJSON(clients, total);
    }

    public String insert(String name, String cpf, String birthDate) {
        String error = Validation.checkField("name", name);
        error += Validation.checkField("cpf", cpf);
        error += Validation.checkField("birthDate", birthDate);
        error += Validation.checkDate(birthDate);
        error += Validation.checkCpf(cpf);

        if(!error.isEmpty()) {
            return error;
        }

        Client client = new Client();
        client.setBirthDate(parseDate(birthDate));
        client.setCpf(cpf);
        client.setName(name);

        try {
            repository.save(client);
        } catch (DataIntegrityViolationException exception) {
            if(exception.getMessage().contains("cpf_UNIQUE")) {
                return "Error this CPF already exist. ";
            }

            return "Error " + exception.getMessage() + ". ";
        } catch (Exception e) {
            return "Error " + e.getMessage() + ". ";
        }

        return "success";
    }

    public String delete(Integer id) {
        String error = Validation.checkField("id", id);

        if(!error.isEmpty()) {
            return error;
        }

        try {
            repository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            return "Error no client found. ";
        } catch (Exception e) {
            return "Error " + e.getMessage() + ". ";
        }

        return "success";
    }

    public String update(Integer id, String name, String cpf, String birthDate) {
        String error = Validation.checkField("id", id);
        error += Validation.checkField("name", name);
        error += Validation.checkField("birthDate", birthDate);
        error += Validation.checkField("cpf", cpf);
        error += Validation.checkDate(birthDate);
        error += Validation.checkCpf(cpf);

        if(!error.isEmpty()) {
            return error;
        }

        try {
            Client client = repository.getOne(id);
            client.setBirthDate(parseDate(birthDate));
            client.setCpf(cpf);
            client.setName(name);
            repository.save(client);
        } catch (DataIntegrityViolationException exception) {
            if(exception.getMessage().contains("cpf_UNIQUE")) {
                return "Error this CPF already exist. ";
            }

            return "Error " + exception.getMessage() + ". ";
        } catch (EntityNotFoundException e) {
            return "Error not found client with id " + id + ". ";
        } catch (Exception e) {
            return "Error " + e.getMessage() + ". ";
        }

        return "success";
    }

    public String updatePart(Integer id, String field, String value) {
        String error = Validation.checkField("id", id);
        error += Validation.checkField("field", field);
        error += Validation.checkField("value", value);

        if(field.equals("birthDate")) {
            error += Validation.checkDate(value);
        } else if(field.equals("cpf")) {
            error += Validation.checkCpf(value);
        }

        if(!error.isEmpty()) {
            return error;
        }

        try {
            Client client = repository.getOne(id);
            if(field.equals("cpf")) {
                client.setCpf(value);
            } else if(field.equals("birthDate")) {
                client.setBirthDate(parseDate(value));
            } else if(field.equals("name")) {
                client.setName(value);
            }

            repository.save(client);
        } catch (DataIntegrityViolationException exception) {
            if(exception.getMessage().contains("cpf_UNIQUE")) {
                return "Error this CPF already exist. ";
            }

            return "Error " + exception.getMessage() + ". ";
        } catch (EntityNotFoundException e) {
            return "Error not found client with id " + id + ". ";
        } catch (Exception e) {
            return "Error " + e.getMessage() + ". ";
        }

        return "success";
    }

    private Date parseDate(String date) {
        try {
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    private String toJSON(ArrayList<Client> clients, int total) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("totalPages", total);
        Gson gson = new Gson();

        JsonArray list = new JsonArray();
        for(Client client : clients) {
            list.add(gson.toJsonTree(client));
        }

        jsonObject.add("clients", list);

        return jsonObject.toString();
    }
}
