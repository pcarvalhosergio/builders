package com.builders.project.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;

public class Validation {

    static public String checkField(String field, Integer value) {
        if(field.equals("pageNumber")) {
            if(value == null || value < 0) {
                return "Parameters not accept " + field + " need be 0 or more. ";
            }
        } else {
            if(value == null || value < 1) {
                return "Parameters not accept " + field + " need be more than 0. ";
            }
        }

        return "";
    }

    static public String checkField(String field, String value) {
        if(value == null || value.isEmpty()) {
            return "Parameters not accept " + field + " is required. ";
        }

        return "";
    }

    static public String checkDate(String date) {
        SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
        try {
            format.parse(date);
        } catch (ParseException e) {
            return "Error date format, correct format is MM/dd/yyyy";
        }

        return "";
    }

    /*Code from https://www.devmedia.com.br/validando-o-cpf-em-uma-aplicacao-java/22097*/
    public static String checkCpf(String CPF) {
        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") ||
                CPF.equals("11111111111") ||
                CPF.equals("22222222222") || CPF.equals("33333333333") ||
                CPF.equals("44444444444") || CPF.equals("55555555555") ||
                CPF.equals("66666666666") || CPF.equals("77777777777") ||
                CPF.equals("88888888888") || CPF.equals("99999999999") ||
                (CPF.length() != 11))
        {
            return "Error cpf invalid, just use numbers. ";
        }

        char dig10, dig11;
        int sm, i, r, num, peso;

        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i=0; i<9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char)(r + 48); // converte no respectivo caractere numerico

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for(i=0; i<10; i++) {
                num = (int)(CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else dig11 = (char)(r + 48);

            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10))) {
                return "";
            } else {
                return "Error cpf invalid. ";
            }
        } catch (InputMismatchException erro) {
            return "Error cpf invalid, just use numbers. ";
        }
    }

}
