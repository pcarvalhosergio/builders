package com.builders.project.controller;

import com.builders.project.service.ClientService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class ClientController {

    ClientService clientService;

    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @RequestMapping(value = "/find", method = { RequestMethod.GET }, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String find(@RequestParam(value = "term", defaultValue = "") String term,
                @RequestParam(value = "size", required=true, defaultValue="0") Integer size,
                @RequestParam(value = "page", required=true, defaultValue="0") Integer page) {
        return clientService.find(term, size, page);
    }

    @RequestMapping(value = "/insert", method = { RequestMethod.POST })
    String insert(@RequestParam(value = "name", defaultValue = "") String name,
                         @RequestParam(value = "cpf", defaultValue = "") String cpf,
                         @RequestParam(value = "birthDate", defaultValue = "") String birthDate) {
        return clientService.insert(name, cpf, birthDate);
    }

    @RequestMapping(value = "/delete", method = { RequestMethod.DELETE }, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String delete(@RequestParam(value = "id", required=true, defaultValue="0") Integer id) {
        return clientService.delete(id);
    }

    @RequestMapping(value = "/update", method = { RequestMethod.PUT }, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String update(@RequestParam(value = "id", required=true, defaultValue="0") Integer id,
                         @RequestParam(value = "name", defaultValue = "") String name,
                         @RequestParam(value = "cpf", defaultValue = "") String cpf,
                         @RequestParam(value = "birthDate", defaultValue = "") String birthDate) {
        return clientService.update(id, name, cpf, birthDate);
    }

    @RequestMapping(value = "/updatePart", method = { RequestMethod.PATCH }, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    String updatePart(@RequestParam(value = "id", required=true, defaultValue="0") Integer id,
                         @RequestParam(value = "field", defaultValue = "") String field,
                         @RequestParam(value = "value", defaultValue = "") String value) {
        return clientService.updatePart(id, field, value);
    }

}
