USE BuildersDB;

DROP TABLE IF EXISTS `client`;

CREATE TABLE `client` (
  `id` int NOT NULL AUTO_INCREMENT,
  `birth_date` datetime DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `cpf` varchar(11) NOT NULL UNIQUE,
  PRIMARY KEY (`id`)
);

SET character_set_client = utf8;
SET character_set_connection = utf8;
SET character_set_results = utf8;
SET collation_connection = utf8_general_ci;
