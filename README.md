# how to install, execute the comands:

$ docker build -t mysql-client .

$ docker run -d -p 3308:3306 --name mysql57 -e MYSQL_ROOT_PASSWORD=RootPassword -e MYSQL_DATABASE=BuildersDB -e MYSQL_USER=MainUser -e MYSQL_PASSWORD=MainPassword mysql-client

# execute jar with the command:
$ java -jar project-0.0.1-SNAPSHOT.jar

# importer builders.postman_collection.json to postman and just execute tests

